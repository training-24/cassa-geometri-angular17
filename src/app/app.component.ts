import { Component } from '@angular/core';
import { RouterLink, RouterOutlet } from '@angular/router';
import { NavbarComponent } from './core/navbar.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, RouterLink, NavbarComponent],
  template: `
   
    <app-navbar />
    <router-outlet />
  `,
  styles: [],
})
export class AppComponent {
  title = 'cassa-geometri-angular17';
}
