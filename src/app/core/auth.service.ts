import { Injectable, signal } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  token = signal<string>('123')

  updateToken() {
    this.token.set(Math.random().toString())
  }
}
