import { JsonPipe } from '@angular/common';
import { Component, computed, effect, signal, untracked } from '@angular/core';

@Component({
  selector: 'app-demo1',
  standalone: true,
  imports: [
    JsonPipe
  ],
  template: `
    <h1
      [style.background-color]="getColor()"
    >Signal {{visible}}</h1>
    <div>{{counter()}}</div>
    
    <div>{{derivedCounter()}}</div>
    
    <button (click)="dec()">-</button>
    <button (click)="inc()">+</button>
    <button (click)="reset()">reset</button>
    
    <button 
      (click)="visible = !visible"
    >toggle visible</button>
    
    {{todosFiltered() | json}}
  `,
})
export default class Demo1Component {
  counter = signal(0)
  derivedCounter = computed(() => this.counter() * 10)
  isZero = computed(() => this.counter() === 0)
  getColor = computed(() => this.isZero()  ? 'red' : 'green')
  todosFiltered = computed(() => this.counter() === 0 ? [] : new Array(this.counter()))

  visible = false;

  todos = signal<any[]>([])

  constructor() {
    effect(() => {
      // const a = untracked(() => this.todos())
      localStorage.setItem('counter', this.counter().toString())
      // ...
    });

  }

  inc() {
    this.counter.set(this.counter() + 1)
  }
  dec() {
    this.counter.update(prev => prev - 1)
  }
  reset() {
    this.counter.set(0)
  }


  getDerivedCounter() {
    console.log('derived')
    return this.counter() * 10
  }

}
