// app.component.ts
import { Component, computed, inject, signal } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AsyncPipe, JsonPipe } from '@angular/common';
import { toSignal } from '@angular/core/rxjs-interop';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { interval } from 'rxjs';
import { User } from '../../model/user';

@Component({
  selector: 'app-demo3',
  standalone: true,
  imports: [JsonPipe, AsyncPipe, ReactiveFormsModule],
  template: `
      <h1>HttpClient Demo</h1>

      <input type="text" [formControl]="input">
      
      @for (user of filteredUsers(); track user.id) {
          <li>{{user.name}}</li>
      }
      
  `,
})
export  default class Demo3Component {
  input = new FormControl() ;
  data = toSignal(this.input.valueChanges)

  users = toSignal(
    inject(HttpClient).get<User[]>('https://jsonplaceholder.typicode.com/users')
  )

  filteredUsers = computed(() => {
    return this.users()?.filter(u => u.name.includes(this.data() || ''))
  })

}
