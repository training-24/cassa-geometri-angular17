import { Component, signal } from '@angular/core';
import { CardComponent } from '../shared/card.component';
import { LeafletComponent } from '../shared/leaflet.component';
import { PanelComponent } from '../shared/panel.component';
import { PicsumComponent } from '../shared/picsum.component';
import { SharedModule } from '../shared/shared.module';
import { SidePanelComponent } from '../shared/side-panel.component';
import { WeatherComponent } from '../shared/weather.component';

@Component({
  selector: 'app-demo4',
  standalone: true,
  imports: [
    CardComponent,
    PicsumComponent,
    PanelComponent,
    WeatherComponent,
    LeafletComponent,
    SidePanelComponent
  ],
  template: `

    <h1> Panel </h1>
    <app-panel
      title="Sono un panel"
      text="bla bla bla"
      disableButton
      value="50"
      size="xl"
      (primaryClick)="doSomething2()"
    />

    <h1>Side Panel</h1>
    <button (click)="visible = !visible">SHOW </button>
    <app-side-panel
      [(show)]="visible"
    />
    
    <pre>{{visible}}</pre>
    
    
    <h1>Leaflet</h1>
    <app-leaflet [coords]="coords" [zoom]="zoom" />
    <button class="btn" (click)="zoom = zoom - 1">-</button>
    <button class="btn" (click)="zoom = zoom + 1">+</button>
    <button class="btn" (click)="coords = [44, 12]">Coords 1</button>
    <button class="btn" (click)="coords = [42, 11]">Coords 2</button>
    <button class="btn" (click)="coords = [45, 10]">Coords 3</button>

    <h1>Weather</h1>
    <app-weather city="trieste" />
    
    <h1>Picsum</h1>
    
    <app-picsum w="50" />
    <app-picsum w="1000" h="400" grayscale/>

   
    
    <hr>
    <h1> Card with service/signal</h1>
    <app-card [title]="myTitle"/>
    <p>
      demo4 works!
    </p>
    <button (click)="doSomething()">x</button>
  `,
  styles: ``
})
export default class Demo4Component {
  zoom = 6;
  coords: [number, number] = [43, 13];
  myTitle = 'ciao'
  pippo = signal(0)

  visible = true

  doSomething() {

    this.myTitle = Math.random().toString()
  }

  doSomething2() {
    window.alert('hello')
  }



  render() {
    console.log('render parent')
  }
}
