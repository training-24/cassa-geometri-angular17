import { AsyncPipe } from '@angular/common';
import { Component } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { interval, Subject, take, takeUntil } from 'rxjs';

@Component({
  selector: 'app-demo5',
  standalone: true,
  imports: [
    AsyncPipe
  ],
  template: `
    <p>
      demo5 works! {{timer | async}}
    </p>
  `,
  styles: ``
})
export default class Demo5Component {
  timer = interval(1000)

  constructor() {
    this.timer
      .pipe(takeUntilDestroyed())
      .subscribe(value => {
        console.log('timer', value)
      })
  }
}
