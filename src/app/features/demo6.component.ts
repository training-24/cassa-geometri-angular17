import { NgComponentOutlet } from '@angular/common';
import { Component, computed, inject, signal, Type, ViewContainerRef } from '@angular/core';
import { CardComponent } from '../shared/card.component';
import { PanelComponent } from '../shared/panel.component';
import { WeatherComponent } from '../shared/weather.component';

@Component({
  selector: 'app-demo6',
  standalone: true,
  imports: [
    NgComponentOutlet
  ],
  template: `
    <p>
      demo6 works!
    </p>
    
    <ng-template *ngComponentOutlet="tpl(); inputs: inputs()"></ng-template>
    
    <button (click)="changeCompo()">Change component</button>
    
    <footer>footer</footer>
  `,
  styles: ``
})
export default class Demo6Component {
  obj = signal<Widget>( {
    tpl: 'card',
    inputs: {
      title: 'titolo della card'
    }
  })

  tpl = computed(() => {
    return COMPONENTS[this.obj().tpl]
  })
  inputs = computed(() => {
    return this.obj().inputs
  })

  changeCompo() {
    this.obj.set({
      tpl: 'weather',
      inputs: {
        city: 'Trieste'
      }
    })
  }
}

export type Widget = {
  tpl: string,
  inputs: any;
}

const COMPONENTS: { [key: string]: Type<any> } = {
  'card': CardComponent,
  'weather': WeatherComponent
}
