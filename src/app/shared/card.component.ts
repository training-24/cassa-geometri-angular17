import { ChangeDetectionStrategy, Component, inject, Input } from '@angular/core';
import { AuthService } from '../core/auth.service';

@Component({
  selector: 'app-card',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [],
  template: `
    <p>
      card works! {{title}} - 
    </p>
    <div>token: from srv {{authService.token()}}</div>
    
  `,
  styles: ``
})
export class CardComponent {
  authService = inject(AuthService)
  @Input() title: string | undefined;




  render() {
    console.log('render card')
  }
}
