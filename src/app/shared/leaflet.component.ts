import { ChangeDetectionStrategy, Component, computed, effect, ElementRef, input, viewChild } from '@angular/core';

import * as L from 'leaflet';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-leaflet',
  standalone: true,
  imports: [],
  template: `
    <div #map class="map"></div>
  `,
  styles: `
    .map { height: 200px; background-color: red }
  `
})
export class LeafletComponent {
  coords = input.required<any>()
  zoom = input<number>(8)

  mapReference = viewChild.required<ElementRef<HTMLElement>>('map')
  map = computed(() => L.map(this.mapReference()?.nativeElement))

  constructor() {
    effect(() => {
        this.map().setView([51.505, -0.09], 13);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png')
          .addTo( this.map());
    });

    effect(() => {
      this.map().setZoom(this.zoom())
    });
    effect(() => {
      this.map().setView(this.coords())
    });
  }
}

