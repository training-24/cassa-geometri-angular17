import {
  booleanAttribute, Component, EventEmitter,
  Input, numberAttribute, output, Output
} from '@angular/core';

@Component({
  selector: 'app-panel',
  standalone: true,
  imports: [],
  template: `
    
    <h1>{{title}} : {{counter}}</h1>
    <em>{{text}}</em>
    
    <button (click)="primaryClick.emit()">Primary button</button>
    
    <button 
      [disabled]="disableButton"
    > Do something </button>
  `,
  styles: ``
})
export class PanelComponent {
  @Input({ required: true }) title: string | undefined

  @Input({ transform: numberAttribute, alias: 'value' })
  counter: number = 0

  @Input({ transform: booleanAttribute })
  disableButton: boolean = false;

  @Input({ transform: (val: string) => {
    return val.toUpperCase()
  }}) text: string | undefined;

  @Input({ transform: (val: 'sm' | 'md' | 'xl') => {
      switch (val) {
        case 'sm': return 50;
        case 'md': return 75;
        default:
        case 'xl': return 100;
      }
    }})
  size = 100

  // @Output() primaryClick = new EventEmitter()
  primaryClick = output<void>();
}
