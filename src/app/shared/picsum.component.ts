import { transition } from '@angular/animations';
import { booleanAttribute, Component, computed, input, numberAttribute } from '@angular/core';

@Component({
  selector: 'app-picsum',
  standalone: true,
  imports: [],
  template: `
    <p>
      picsum works! {{isTooSmall()}} {{width()}} {{height()}} {{grayscale()}}
    </p>
    @if(isTooSmall()) {
      size too small
    } @else {
      <img [src]="imageUrl()" alt="">
    }
  `,
  styles: ``
})
export class PicsumComponent {
  width = input(300, { alias: 'w', transform: numberAttribute})
  height = input(200, { alias: 'h', transform: numberAttribute})
  grayscale = input(false, { transform: booleanAttribute })

  imageUrl = computed(() => {
    const grayscale = this.grayscale() ? '?grayscale' : ''
    return `https://picsum.photos/${this.width()}/${this.height()}${grayscale}`
  })

  isTooSmall = computed(() => {
    return this.width() < 100 || this.height() < 100
  })

}
