import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card.component';
import { PicsumComponent } from './picsum.component';

export const COMPONENTS = [
  CardComponent,
  PicsumComponent
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CardComponent,
    PicsumComponent
  ],
  exports: [
    CardComponent,
    PicsumComponent
  ]
})
export class SharedModule { }
