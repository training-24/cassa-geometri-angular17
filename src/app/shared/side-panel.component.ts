import { NgIf } from '@angular/common';
import { Component, EventEmitter, input, Input, model, Output } from '@angular/core';

@Component({
  selector: 'app-side-panel',
  standalone: true,
  imports: [
    NgIf
  ],
  template: `
  
    ...{{show()}}
    <div *ngIf="show()">
      SIDE PANEL
    </div>
    
    <button (click)="close()">Close</button>
    
  `,
  styles: ``
})
export class SidePanelComponent {
  show = model(true)

  close() {
    this.show.set(false)
  }

}
