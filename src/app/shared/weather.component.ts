import { JsonPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, computed, effect, inject, input, signal } from '@angular/core';
import { Meteo } from '../../model/meteo';

export const BASEURL = 'https://api.openweathermap.org/data/2.5/weather?units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534&q='
@Component({
  selector: 'app-weather',
  standalone: true,
  imports: [
    JsonPipe
  ],
  template: `
    <p>
      weather works {{ city() }}
    </p>

    @if (meteo(); as meteo) {
      <div style="background-color: red; padding: 20px">
        {{ meteo.main.temp }}
      </div>
    }
    @if (temperature()) {
      <div style="background-color: cyan; padding: 20px">
        {{ temperature() }}
      </div>
    }
  `,
  styles: ``
})
export class WeatherComponent {
  city = input.required<string>()
  http = inject(HttpClient)
  meteo = signal<Meteo | null>(null)
  temperature = computed(() => this.meteo()?.main?.temp)

  constructor() {
    effect(() => {
      this.http.get<Meteo>(`${BASEURL}${this.city()}`)
        .subscribe(res => {
          this.meteo.set(res)
        })
    });
  }

}
